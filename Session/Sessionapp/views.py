from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
site_visits = 0
def home_page(r):
    global site_visits
    site_visits += 1
    if r.user.is_active:
        my_cnt = r.session.get("ur_cnt", 0)
        if my_cnt == 0:
            r.session['ur_cnt'] = 1
        else:
            r.session['ur_cnt'] += 1
        return render(r,"index.html", {'site_cnt': site_visits, 'ur_cnt': r.session['ur_cnt']})
    return render(r,"index.html", {'site_cnt': site_visits, 'ur_cnt': 0})

def log_in(r):
    u = authenticate(username='admin', password='123')
    login(r, u)
    return redirect('home')

def log_out(r):
    logout(r)
    return redirect('home')

def dashboard(r):
    return render(r, 'dash.html', {'session_variable': r.session['name']})

